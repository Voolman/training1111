import 'package:training_final_8/data/models/on_boarding_model.dart';
import 'package:training_final_8/data/repository/on_boarding_repoistory.dart';

List<OnBoardingModel> getCurrentOnBoarding(){
  List<OnBoardingModel> allBoardings = getOnBoardingModels().map((e) => OnBoardingModel.fromJson(e)).toList();
  return allBoardings;
}