import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:training_final_8/data/models/on_boarding_model.dart';
import 'package:training_final_8/data/repository/on_boarding_repoistory.dart';

import '../../domain/on_boarding_presenter.dart';



class OnBoarding extends StatefulWidget {
  const OnBoarding({super.key});

  @override
  State<OnBoarding> createState() => _OnBoardingState();
}
OnBoardingModel onBoarding = getCurrentOnBoarding()[0];
class _OnBoardingState extends State<OnBoarding> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color.fromARGB(255, 72, 178, 231),
                Color.fromARGB(255, 0, 118, 177)
              ]
          )
        ),
        child: (onBoarding!.index == '0') ? Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 121, left: 47, right: 61),
              child: Text(
                onBoarding!.text,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            SvgPicture.asset('assets/first.svg')
          ],
        ) : Column(),
      ),
    );
  }
}