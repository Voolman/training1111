import 'package:flutter/material.dart';

var theme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      fontWeight: FontWeight.w900,
      fontSize: 30,
      color: Color.fromARGB(255, 236, 236, 236),
      height: 35.22 / 30
    )
  )
);
