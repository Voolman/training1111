List<Map<String, String>> getOnBoardingModels(){
  return [
    {'index': '0', 'image': 'assets/first.svg', 'text': 'Добро пожаловать', 'subtext': ''},
    {'index': '1', 'image': 'assets/second.svg', 'text': 'Начнем путешествие', 'subtext': 'Умная, великолепная и модная коллекция Изучите сейчас'},
    {'index': '2', 'image': 'assets/third.svg', 'text': 'У вас есть сила, чтобы', 'subtext': 'В вашей комнате много красивых и привлекательных растений'}
  ];
}