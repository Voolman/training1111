class OnBoardingModel {
  final String index;
  final String image;
  final String text;
  final String subtext;

  OnBoardingModel(
      {required this.index,
      required this.image,
      required this.text,
      required this.subtext});

  static OnBoardingModel fromJson(Map<String, String> json) {
    return OnBoardingModel(
        index: json['index']!,
        image: json['image']!,
        text: json['text']!,
        subtext: json['subtext']!
    );
  }

  static Map<String, String> toJson(OnBoardingModel json){
    return {'index': json.index, 'image': json.image, 'text': json.text, 'subtext':json.subtext};
  }
}
